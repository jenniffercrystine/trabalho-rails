Rails.application.routes.draw do
  resources :posts
  root 'static_pages#home'

  
  resources :comment

  resources :users, except: [:new], path: 'usuarios'

  get '/cadastro', to: 'users#new'

  get '/todos', to: 'users#feed'

  get '/perfil', to: 'users#show'

  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  get '/comments', to: 'comment#index'
  post '/comments', to: 'comment#index'
  get '/comment/new', to: 'comment#new'

  get '/new', to: 'users#index'

  get '/amigos', to: 'friendships#index'

  resources :friendships


  patch 'like/:post_id', to: "posts#like", as: :like
  patch 'like/:post_id', to: "posts#dislike", as: :dislike


  

  



  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
