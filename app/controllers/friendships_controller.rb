class FriendshipsController < ApplicationController

	def index
		@friendships = Friendship.all
		@users = User.all
		@posts = Post.all
		@user = Friendship.all
	end
    

	def create
		@friendship = current_user.friendships.build(:friend_id => params[:friend_id])
		if @friendship.save
			flash[:notice] = "Você tem um novo amigo"
			redirect_to root_url
		else
			flash[:notice] = "Essa amizade não é possível"
			redirect_to root_url
		end
	end

	def show
		
	end


	def destroy
		@friendship = current_user.friendships.find(params[:id])
		@friendship.destroy
		flash[:notice] = "Amigo removido </3"
		redirect_to current_user
	end


  
end
