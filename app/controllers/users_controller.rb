class UsersController < ApplicationController
    before_action :nao_logado, except: [:new, :create]  #impede pessoas que não estão logadas de acessar páginas que só pessoas logadas podem
    before_action :logado, only: [:new] #impede que pessoas logadas façam o login novamente

    def index
        @users = User.all
    end

    def feed
        @users = User.all
        @posts = Post.all
        @friendships = Friendship.all
        @user = current_user

    end


    def new
        @user = User.new
    end

    def create
        @user = User.new(user_params)
        if @user.save
            log_in @user
            redirect_to @user
        else
            render 'new'
        end
    end

    def index
        @users = User.all
    
    end

    def destroy
		if current_user.id == user_id
            @user.destroy #quando eu chamo o destroy nesse caso, eu preciso fazer algo para especificar que é o comentário em questão ou não precisa
            redirect_to login_path
		else
			flash[:notice] = "Você não tem autorização para isso!"
			redirect_to todos_path
		end
	end

    def show
        @user = User.find(params[:id])
        Friendship.where(user_id: @current_user.id)
    end

    private
        def user_params
            params.require(:user).permit(:name, :email, :password, :password_confirmation)
        end

        def nao_logado
            if !logged_in?
                redirect_to root_path
            end
        end

        def logado
            if logged_in?
                redirect_to current_user
            end
        end



end
