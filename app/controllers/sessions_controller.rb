class SessionsController < ApplicationController

    before_action :logado, only: [:new]
    before_action :nao_logado, only: [:destroy]

    def new
    end


    def create
        user = User.find_by(email: params[:session][:email])
        if user && user.authenticate(params[:session][:password])
            log_in(user)
            redirect_to user
            #A fazer
        else
            render 'new'
        end
    end

    def destroy
        log_out
        redirect_to root_path
    end

    def logado
        if logged_in?
            redirect_to current_user
        end
    end

    def nao_logado
        if !logged_in?
            redirect_to login_path
        end
    end
end
