	class CommentController < ApplicationController


	def index
	end

	def new
		@comment = Comment.new
	
	end

	def create
		@comment = Comment.new(comment_params)

		respond_to do |format|
			if @comment.save
				flash[:notice] = "Seu comentário foi postado!"
				redirect_to post_path
			else
				redirect_to new_comment_path
			end
		end
				
	end

	def update
		respond_to do |format|
			if @comment.update(comment_params)
				flash[:notice] = "Comentário atualizado!"
				redirect_to post_path
				
			else
				render 'edit'
			end
		end

	end

	def destroy
		if current_user.id == @comment.user_id
			@comment.destroy #quando eu chamo o destroy nesse caso, eu preciso fazer algo para especificar que é o comentário em questão ou não precisa
		else
			flash[:notice] = "Você não tem autorização para isso!"
			redirect_to post_path
		end
	end


	private

	def set_comment
	@comment = Comment.find(params[:id])
	end

	def comment_params
	params.require(:comment).permit(:user_id, :post_id, :comment_box)

	end


end
