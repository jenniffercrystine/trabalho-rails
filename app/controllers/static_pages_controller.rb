class StaticPagesController < ApplicationController

    before_action :logado, only: [:home]


    def home
    end


    private
    def logado
        if logged_in?
            redirect_to current_user
        end
    end
end
